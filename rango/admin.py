from django.contrib import admin
from rango.models import Category, Page, UserProfile

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug':('name',)}

class PageAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'url', 'views')

admin.site.register(Category, CategoryAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(UserProfile)
# Register your models here.




# Within your new PageAdmin class, add list_display = ('title', 'category', 'url').
#Finally, register the PageAdmin class with Django’s admin interface.
#You should modify the line admin.site.register(Page).
#Change it to admin.site.register(Page, PageAdmin) in Rango’s admin.py file.
