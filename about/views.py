from django.shortcuts import render
from django.http import HttpResponse

def about(request):
    context_dict = {'aboutmessage': "Rango says here is the about page."}
    return render(request, 'rango/about.html', context=context_dict)
